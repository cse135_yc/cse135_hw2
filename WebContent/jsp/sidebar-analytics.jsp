<%@page
	import="java.util.List"
	import="helpers.*"%>
<%
	List<CategoryWithCount> categories = CategoriesHelper
			.listCategories();
%>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="bottom-nav">
            <h4> Options </h4>
            <!-- Put your part 2 code here -->
            <%
			String CorS = request.getParameter("C_S");
			CorS = (CorS != null)? CorS : "";
			String AorTop = request.getParameter("A_Top");
			AorTop = (AorTop != null)? AorTop : "";
			String category = request.getParameter("cname");
			category = (category != null)? category : "";
			String actionName = request.getParameter("actionName");
            actionName = (actionName != null) ? actionName : "analytics";
            Integer cid = -1;
			%>
            <form action="<%=actionName%>" method="post">
	            
				<br>Row Option : <select
					id="C_S"
					name="C_S"
				>
				<%
				String[] CS = new String[2];
				CS[0] = "Customers";
				CS[1] = "States";
				for (int i = 0; i < 2; i++) {
					if (CS[i].equals(CorS)) {
				%>
					<option selected="selected">
				<%
					} else {
				%>
					<option>
				<%
					}
				%>
					<%=CS[i]%>
					</option>
				<%
				}
				%>
				</select>
				<br>Order Option : <select
					id="A_Top"
					name="A_Top"
				>
				<%
				String[] AT = new String[2];
				AT[0] = "Alphabetical";
				AT[1] = "Top K";
				for (int i = 0; i < 2; i++) {
					if (AT[i].equals(AorTop)) {
				%>
					<option selected="selected">
				<%
					} else {
				%>
					<option>
				<%
					}
				%>
					<%=AT[i]%>
					</option>
				<%
				}
				%>
				</select>
				<br>Category Option: <select
					id = "cname"
					name = "cname"
				>
				<%
			    for (CategoryWithCount cwc : categories) {
			    	cid = cwc.getId();
			    	if (cid.toString().equals(category)) {			    		
			    %>
			    		<option value="<%=cwc.getId()%>" selected="selected">
			    <%
			    	} else {
			    %>
			    		<option value="<%=cwc.getId()%>">
			    	<%
			    	}
			    	%>
			    	<%=cwc.getName()%>
			    	</option>
			    <%
			    }
			    %>
			    <%
			    if (category.equals("")||category.equals("-1")) {
			    %>
			    	<option value="-1" selected="selected">
			    <%
			    } else {
			    %>
			    	<option value="-1">
			    <%
			    }
			    %>
			    <%="All"%>
			    </option>
				</select>
				<br><br><input type="submit" name="run" value="Run Query">
			</form>
		</div>
	</div>
</div>