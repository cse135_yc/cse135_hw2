<%@page
	import="java.util.List"	
	import="java.util.Date"
	import="helpers.*"%>
	

<%if(request.getParameter("run")!=null || request.getParameter("next")!=null){ 
	
	if(request.getParameter("run")!=null){
		session.setAttribute("pd_offset", 0);
		session.setAttribute("CS_offset", 0);
	}
	
	List<List<String>> table = AnalyticsHelper
			.listSales(request, session);
	List<String> colHeader = table.get(0);
	List<String> rowHeader = table.get(1);
	List<String> sales = table.get(2);
%>
<table
    class="table table-striped"
    align="center">
    <thead>
        <tr>
            <th width="10%"></th>
            
            <%
            for(String productName : colHeader){%>
            	<th width="15%"><%=productName%></th>
            <%} %>
            
        </tr>
    </thead>
    <tbody>
    	<%
    		Integer times = 0;
    		for(String C_SName : rowHeader){ %>
    		<tr>
    			<th width="10%"><B><%=C_SName%></B></th>
    			<%
    			for (int i = times*colHeader.size(); i < times*colHeader.size()+colHeader.size(); i++) {
    			%>
    			<td width="10%"><%="$" + sales.get(i)%></td>
        		<%	
    			}
    			times += 1;
    			%>   			
    		</tr>
    	<%}%>
    </tbody>
    
</table>

<form action="analytics">
    <%
    if(AnalyticsHelper.cs_hasNext()){
    	String C_S = request.getParameter("C_S");
    %>
    	<input
        type="submit"
        name="next"
        value="Next 20 <%=C_S%>"/>
        <%} 
    if(AnalyticsHelper.pd_hasNext()){%>
        <input
        type="submit"
        name="next"
        value="Next 10">
        <%} %>
        <input
        type="hidden"
        name="C_S"
        value="<%=request.getParameter("C_S")%>">
        <input
        type="hidden"
        name="A_Top"
        value="<%=request.getParameter("A_Top")%>">   
        <input
        type="hidden"
        name="cname"
        value="<%=request.getParameter("cname")%>">
</form>

<%}%>