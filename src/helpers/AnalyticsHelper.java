package helpers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import java.util.Date;

import static java.lang.System.*;
import static java.lang.System.out;

import javax.servlet.http.HttpServletRequest;

public class AnalyticsHelper {
	
	private static boolean pd_hasNext = true;
	private static boolean cs_hasNext = true;
	
	public static List<List<String>> listSales(HttpServletRequest request, HttpSession session) {
        List<List<String>> table = new ArrayList<List<String>>();
        List<String> col = new ArrayList<String>();
        List<String> row = new ArrayList<String>();
        List<String> sales = new ArrayList<String>();
        
        Connection conn = null;
        Statement colstmt = null, rowstmt = null, cellstmt = null, tempstmt = null, colspstmt=null;
        ResultSet colrs = null,rowrs = null, cellrs = null;
        String ctcolFilter = "", productFilter = "", C_SFilter = "", atFilter = "";
        String csFrom = "", csLast="", ctunique = "", ctrowFilter="";
        String A_TOP = request.getParameter("A_Top");
        String C_S = request.getParameter("C_S");
        String[] offsetFilter = nextSales(request, session);
        String CS_offset = offsetFilter[0], pd_offset = offsetFilter[1];
        try {
        	if (!request.getParameter("cname").equals("-1")) {
                Integer cid = Integer.parseInt(request.getParameter("cname"));
                if (cid != null){
                	ctrowFilter = " LEFT JOIN (SELECT * FROM products "
                					+ "WHERE products.cid = " + cid
                					+ ") AS p ON s.pid = p.id";
                	ctcolFilter = "WHERE products.cid = " + cid;
                	ctunique = "LEFT JOIN (SELECT* FROM products WHERE products.cid="
                			+ cid
                			+ ") AS p ON sales.pid = p.id";
                }
        	}
        } catch (Exception e) {
        }
        try {
        	
            if (A_TOP == null || A_TOP.equals("Alphabetical")){
            		
            		productFilter = "p.name";
            		
            		if (C_S == null || C_S.equals("Customers")) {
                		C_SFilter = "u";
                		csFrom = "FROM sales AS s RIGHT JOIN (SELECT * FROM users ORDER BY users.name "
                				+ CS_offset 
                				+ " LIMIT 20) AS u ON s.uid = u.id "
                				+ ctrowFilter;
                	} else {
                		C_SFilter = "states";
                		csFrom = "FROM (SELECT * FROM states ORDER BY states.name "
                				 + CS_offset
                				 + " LIMIT 20) AS states LEFT JOIN users AS u ON states.id=u.state "
                				 + "LEFT JOIN sales AS s ON s.uid = u.id "
                				 + ctrowFilter;
                	}
            		atFilter = C_SFilter + ".name";
            }
            else {
            	atFilter = "SUM(s.price*s.quantity) DESC";
            	productFilter = "SUM(s.price*s.quantity) DESC";
            	if (C_S == null || C_S.equals("Customers")) {
            		C_SFilter = "u";
            		csFrom = "FROM users AS u LEFT JOIN sales AS s ON s.uid = u.id "
            				+ ctrowFilter;
            		csLast = "NULLS LAST "
            				+ CS_offset
            				+ " LIMIT 20";
            	} else {
            		C_SFilter = "states";
            		csFrom = "FROM states LEFT JOIN users AS u ON u.state = states.id "
            				+ "LEFT JOIN sales AS s ON s.uid = u.id "
            				+ ctrowFilter;
            		csLast = "NULLS LAST" + CS_offset + " LIMIT 20";
            	}
            }
        } catch (Exception e) {
        }
        try {
            try {
                conn = HelperUtils.connect();
            } catch (Exception e) {
                System.err.println("Internal Server Error. This shouldn't happen.");
                return new ArrayList<List<String>>();
            }
            colstmt = conn.createStatement();
            tempstmt = conn.createStatement();
            rowstmt = conn.createStatement();
            cellstmt = conn.createStatement();
            colspstmt = conn.createStatement();
            String colquery = "CREATE TEMPORARY TABLE MyTempTable AS" 
            					+ " SELECT p.name AS product, SUM(s.price*s.quantity) AS sum, p.id AS id"
            					+ " FROM (SELECT * FROM products "
            					+ ctcolFilter
            					+ " ORDER BY products.name "
            					+ pd_offset
            					+ " LIMIT 10) AS p LEFT JOIN sales AS s ON s.pid = p.id" 
            					+ " GROUP BY p.name, p.id"
            					+ " ORDER BY "
            					+ productFilter;
            String rowquery;
            if (A_TOP == null || A_TOP.equals("Alphabetical")) {
            	rowquery = "SELECT " + C_SFilter + ".name, SUM(s.price*s.quantity) AS sum, " 
    					+ C_SFilter + ".id AS id "
    					+ csFrom
    					+ " GROUP BY " + C_SFilter + ".name, " + C_SFilter + ".id"
    					+ " ORDER BY " + atFilter + " "
    					+ csLast;
            }
            else {
            	if (C_S == null || C_S.equals("Customers")) {
            		
            		rowquery = "SELECT u.name,s.sum AS sum,u.id"
            				+ " FROM users AS u "
            				+ " LEFT JOIN (SELECT sales.uid,SUM(sales.price*sales.quantity) AS sum "
            				+ " FROM sales " + ctunique
            				+ " GROUP BY sales.uid)AS s ON s.uid = u.id "
            				+ " ORDER BY s.sum DESC "
            				+ " NULLS LAST "
            				+ CS_offset
            				+ "LIMIT 20";
            	}
            	else {
            		rowquery = "SELECT " + C_SFilter + ".name, SUM(s.price*s.quantity) AS sum, " 
        					+ C_SFilter + ".id AS id "
        					+ csFrom
        					+ " GROUP BY " + C_SFilter + ".name, " + C_SFilter + ".id"
        					+ " ORDER BY " + atFilter + " "
        					+ csLast;
            	}
            }
             
            String key = "ALTER TABLE MyTempTable ADD PRIMARY KEY (id)";
            long time1=new Date().getTime();
            colstmt.executeUpdate(colquery);
            colspstmt.executeUpdate(key);
            long time2=new Date().getTime();
            out.println("data for col");
            out.println(time2-time1);
            long time3 = new Date().getTime();
            rowrs = rowstmt.executeQuery(rowquery);
            long time4=new Date().getTime();
            out.println("data for row");
            out.println(time4-time3);
            colrs = tempstmt.executeQuery("SELECT * FROM MyTempTable");
            while (colrs.next()) {
            	String productName = colrs.getString(1);
                String trunName = productName.length()>10? productName.substring(0, 10):productName;
                col.add(trunName + "\n($" + colrs.getInt(2) + ")");
            }
            long time5 = new Date().getTime();
            while (rowrs.next()) {
            	Integer scID =rowrs.getInt(3);
            	String cellquery;
            	if (C_S == null || C_S.equals("Customers")) {
            		cellquery = "SELECT SUM(s.price*s.quantity) "
                			+ "FROM (SELECT* FROM sales WHERE sales.uid="+scID
                			+ ") AS s RIGHT JOIN MyTempTable AS m ON s.pid=m.id"
                			+ " GROUP BY m.product";
            	}
            	else {
            		cellquery= "SELECT SUM(s.price*s.quantity) "
            				+ "FROM MyTempTable AS m LEFT JOIN sales AS s ON s.pid=m.id "
            				+ "LEFT JOIN (SELECT*FROM users AS u WHERE u.state=" + scID
            				+ ") AS u ON s.uid=u.id"
            				+ " GROUP BY m.product";
            	}
            	
            	cellrs = cellstmt.executeQuery(cellquery);
            	
            	while (cellrs.next()) {
            		Integer temp = cellrs.getInt(1);
                    sales.add(temp.toString());
            	}
                row.add(rowrs.getString(1) + " ($" + rowrs.getInt(2) + ")");
            }
            long time6 = new Date().getTime();
        	out.println("time for cell");
        	out.println(time6-time5);
            table.add(col);
            table.add(row);
            table.add(sales);
            return table;
        } catch (Exception e) {
            System.err.println("Some error happened!<br/>" + e.getLocalizedMessage());
            return new ArrayList<List<String>>();
        } finally {
            try {
                colstmt.close();
                rowstmt.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
	}
	
	public static String[] nextSales(HttpServletRequest request, HttpSession session){	
		pd_hasNext = true;
		cs_hasNext = true;
		int C_SNum = 0, productsNum = 0;
        Connection conn = null;
        Statement csstmt = null, pdstmt = null;
        ResultSet csrs = null, pdrs = null;
        String offsetFilter[] = new String[2];
        String C_S = request.getParameter("C_S");
        String next = request.getParameter("next");
        try {
            try {
                conn = HelperUtils.connect();
            } catch (Exception e) {
                System.err.println("Internal Server Error. This shouldn't happen.");
                return new String[2];
            }

        		Integer	CS_offset = (Integer)session.getAttribute("CS_offset");
            	csstmt = conn.createStatement();

                if(C_S == null || C_S.equals("Customers")){
                	csrs = csstmt.executeQuery("SELECT COUNT(*) FROM users");
                }
                else{
                	csrs = csstmt.executeQuery("SELECT COUNT(*) FROM states");
                }
                if(csrs.next())
                	C_SNum = csrs.getInt(1);          	
            	if(next!=null && next.contains("Next 20")){
            		CS_offset += 20;
            		session.setAttribute("CS_offset", CS_offset);
            	}
            	if(CS_offset + 20 >= C_SNum){
            		cs_hasNext = false;
            	}
            
            	Integer pd_offset = (Integer)session.getAttribute("pd_offset");
            	pdstmt = conn.createStatement();
            	out.println(request.getParameter("cname"));

            	if (!request.getParameter("cname").equals("-1")) {
                    Integer cid = Integer.parseInt(request.getParameter("cname"));
                    if (cid != null){
                    	pdrs = pdstmt.executeQuery("SELECT COUNT(*) FROM products WHERE products.cid=" + cid);
                    }
                    else{
                		pdrs = pdstmt.executeQuery("SELECT COUNT(*) FROM products");
                	}
                }
            	else{
            		pdrs = pdstmt.executeQuery("SELECT COUNT(*) FROM products");
            	}
            	if(pdrs.next())
            		productsNum = pdrs.getInt(1);
            	if(next!=null && next.equals("Next 10")){
            		pd_offset += 10;
            		session.setAttribute("pd_offset", pd_offset);
            	}
            	if(pd_offset + 10 >= productsNum){
            		pd_hasNext = false;
            	}
            	
            offsetFilter[0] = " OFFSET " + CS_offset.toString();
            offsetFilter[1] = " OFFSET " + pd_offset.toString();
    		return offsetFilter;
    		
        } catch (Exception e) {
            System.err.println("Some error happened!<br/>" + e.getLocalizedMessage());
            return new String[2];
        } finally {
            try {
                csstmt.close();
                pdstmt.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
	}
	
	public static boolean pd_hasNext() {return pd_hasNext;}
	public static boolean cs_hasNext() {return cs_hasNext;}
	
}